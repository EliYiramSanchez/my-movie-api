from models.movie import Movie as MovieModel
from schemas.movie import Movie

class MovieService():

    def __init__(self, db) -> None:
        self.db = db

    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return result
    
    def get_movie(self, id):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result
    
    def get_movies_by_category_and_year(self, category_movie, year_movie):
        result = self.db.query(MovieModel).filter(MovieModel.category == category_movie and MovieModel.year == year_movie).all()
        return result
    
    def create_movie(self, movie: Movie):
        #MovieModel: Entidad de la DDBB
        new_movie = MovieModel(**movie.dict()) #guardamos todos los parámetros como diccionario
        self.db.add(new_movie)
        self.db.commit()
        return
    
    def update_movie(self, id: int, data_to_ubdate: Movie):
        movie = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        movie.title = data_to_ubdate.title
        movie.overview = data_to_ubdate.overview
        movie.year = data_to_ubdate.year
        movie.rating = data_to_ubdate.rating
        movie.category = data_to_ubdate.category
        self.db.commit()
        return

    def delete_movie(self, id: int):
        movie = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        self.db.delete(movie)
        self.db.commit()
        return
        