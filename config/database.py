import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sqlite_database_name = "../database.sqlite"

#Leemos el directorio de este archivo
basedir = os.path.dirname( os.path.realpath(__file__) )

#Creamos una DDBB mediante SQLITE con una URL que une ambas variables anteriores como nombre
database_url = f"sqlite:///{os.path.join(basedir, sqlite_database_name)}"

#creamos el motor de la base de datos
engine = create_engine(database_url, echo= True)

#Se crea session para conectarse a la base de datos, se enlaza con el comando “bind” y se iguala a engine
Session = sessionmaker(bind= engine)

#BASE Sirve para manipular todas las tablas de la base de datos
Base = declarative_base()