from pydantic import BaseModel, Field
from typing import Optional, List
import datetime

class Movie(BaseModel):
    id: Optional[int] = None 
    title: str = Field(min_length=5, max_length=15) #validacion de longitud
    overview: str = Field(min_length=15, max_length=50)
    year: int = Field(le=datetime.date.today().year)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=5, max_length=15)

    class Config:
        schema_extra = {
            "Example": {
            "id": 1,
            "title": "Mi Pelicula",
            "overview": "Descripción de la película",
            "year": 2022,
            "rating": 9.8,
            "category": "Opcional"
            }
        }