from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
import datetime
import json
from fastapi.security import HTTPBearer
from config.database import Session
from models.movie import Movie as MovieModel

from fastapi.encoders import jsonable_encoder

from middlewares.jwt_bearer import JWTBearer

from services.movie import MovieService
from schemas.movie import Movie #SCHEMA DE BASEMODEL

from fastapi import APIRouter
movie_router = APIRouter()



#GET ALL MOVIES
@movie_router.get('/movies', tags=['movies'], response_model= List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:

    db = Session() #creamos en la variable la instancia de Sesión
    #results = db.query(MovieModel).all()
    results = MovieService(db).get_movies() #Consultamos todos los datos de la tabla
    #convertimos la clase MovieMovel en un objeto con JSONABLE_ENCODER...
    
    return JSONResponse(status_code= 200, content= jsonable_encoder(results) )

#ONLY ONE MOVIE
@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie, status_code=200) #Parametro de ruta: {id}
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie: #Parametro de Ruta "PATH"
    
    db = Session()
    #result = db.query(MovieModel).filter( MovieModel.id == id ).first()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(content= {'message': 'Item Not found'}, status_code=404)
    return JSONResponse(content= jsonable_encoder(result), status_code=200)

#MOVIES BY CATEGORY OR YEAR
@movie_router.get('/movies/', tags=['movies'], response_model= List[Movie], status_code=200) #Parametro de Query: Categoria y Año
def get_movies_by_query(category_movie:str = Query(min_length=5 , max_length=15), year_movie:int = Query(ge=1, le=2025)) -> List[Movie]:
    
    db = Session()
    #results = db.query(MovieModel).filter( MovieModel.category == category_movie and MovieModel.year == year_movie ).all()
    results = MovieService(db).get_movies_by_category_and_year(category_movie, year_movie)
    if not results:
        return JSONResponse(content= {'message': 'Item Not found'}, status_code=404)
    return JSONResponse(content= jsonable_encoder(results), status_code=200)

#CREATE A MOVIE   -> List[Movie]
@movie_router.post('/movies', tags=['movies'], response_model= dict, status_code=201)
def create_movie(movie: Movie)-> dict:
    db = Session() #Creamos la sesión para conectarse a la DDBB
    MovieService(db).create_movie(movie)
    return JSONResponse(content= {"message": "Se ha registrado la pelicula"}, status_code=201)
    # return 


#UPDATE A MOVIE   , response_model= List[Movie]
@movie_router.put('/movies/{id}', tags=['movies'], response_model= dict, status_code=200)
def update_movie(id: int, movie: Movie)-> dict:

    db = Session()
    result = MovieService(db).get_movie(id)
    
    if not result:
        return JSONResponse(content= {'message': 'Item Not found'}, status_code=404)
    
    MovieService(db).update_movie(id, movie) #servicio para actualizar la Movie
    return JSONResponse(content= {"message": "Se ha actualizado la pelicula"}, status_code=200)

@movie_router.delete('/movies', tags=['movies'], response_model= dict, status_code=200)
def delete_movie(id: int)-> dict:
    
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).first()

    if not result:
        return JSONResponse(content= {'message': 'Item Not found'}, status_code=404)

    MovieService(db).delete_movie(id)
    return JSONResponse(content= {"message": "Se ha eliminado la pelicula"}, status_code=200)

